# Picom Blur

Basic `picom` blur configuration with `dual_kawase` enabled by default.

# Setup

Copy the `picom.conf` file to a location of choice, e.g. `~/.config/picom/picom.conf`.

Prior to enabling picom, disable your default compositor and kill its process. Run `picom -b --experimental-backends --config ~/.config/picom.conf`. Applications which natively support opacity will be automatically detected and blurred (`Terminator` is one such example of this).

It is recommended to enable `picom` on startup for best effect. An XFCE autostart entry is included as an example which can be copied to `~/.config/autostart`. Note that an absolute path is given for the configuration file's location, this appears to be a requirement due to quarks with XFCE.
